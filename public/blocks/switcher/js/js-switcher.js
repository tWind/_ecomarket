function switcher() {
	var switcher = {
		options: {
			$block: '.js-switcher',
			$bindings: '.js-switcher-button'
		},
		run: function() {
			console.log(this.options.$block)
			$.each($(this.options.$block), function(index, el) {
				var $block = $(el);
				var $elems = $block.find(switcher.options.bindings);

				$elems.on('click', function(e) {
					e.preventDefault();

					$elems.removeClass('active');
					$(this).addClass('active');
				})

				$block.css({
					marginLeft: -$(this).width()/2 + $(this).height()/2,
					marginTop: -$(this).height()/2
				})
			});
		}
	}

	switcher.run();
}
