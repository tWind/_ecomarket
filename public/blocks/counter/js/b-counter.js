function counter() {
	console.log('hi')

	var options = {
		block: '.js-counter'
	}

	$('.js-counter-start').on('click', function() {
		var $elem = $(this);
		var $parent = $elem.closest(options.block);


		$parent.addClass('active');
		$elem.css({
			left: $elem.offset().left,
			top: $elem.offset().top,
			position: 'fixed'
		})
		$elem
			.animate({
				left: 1120,
				top: 20
			}, 900, function() {
				$elem.attr('style', '');
			}
		);

		return false;
	});


	$('.js-counter-minus').on('click', function() {
		var $el = $(this);
		var $parent = $el.closest('.js-counter-pane');
		var $viewPort = $parent.find('span');
		var value = +$viewPort.text();

		value > 1
			? $viewPort.text(value-=1)
			: $parent.closest(options.block).removeClass('active');

		return false;
	});
	$('.js-counter-plus').on('click', function() {
		var $el = $(this);
		var $parent = $el.closest('.js-counter-pane');
		var $viewPort = $parent.find('span');
		var value = +$viewPort.text();

		$viewPort.text(value+=1);

		return false;
	});
};