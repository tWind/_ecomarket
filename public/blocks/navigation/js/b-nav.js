(function() {


	$.fn.navigation = function() {
		var $nav = $(this);
		var $links = $nav.find('a');

		var settings = {
			"showY": 268
		}

		var options = $.extend({}, settings, $nav.data('nav-options'));

		$links.on('click', function(e) {
			e.preventDefault();
		});

		$(window).on('scroll', function() {

			var top = $(document).scrollTop();
			if (top > options.showY)
				$nav.addClass('fixed');
			else
				$nav.removeClass('fixed');
		});

		$('.arcticmodal-container').on('scroll', function() {
			var top = $(this).scrollTop();
			if (top > $options.showY) {
				$nav.addClass('fixed');
				$('.js-fix').addClass('fixed');
			}
			else {
				$nav.removeClass('fixed');
				$('.js-fix').removeClass('fixed');
			}
		});

		console.log(options);

	}

})(jQuery);

function bindMenu() {
	var $block = $('.js-nav');
	$.each($block, function() {
		var $el = $(this);

		$el.navigation();
	});
}

bindMenu();

