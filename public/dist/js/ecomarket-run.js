function setCatalogueBehavior() {
	var options = {
		block: '.js-catalogue',
		links: '.js-catalogue-add',
		modal: '.js-catalogue-modal'
	}

	var $block = $(options.block);
	var $links = $block.find(options.links);
	var $modal = $block.find(options.modal);



	$links.on('click', function(e) {
		e.preventDefault();

		var $parent = $(this).parent();
		$links.parent().not($parent).removeClass('active');

		$parent.toggleClass('active');
	});

	$(document).click(function(e) {
		var $el = $(event.target);

		if ($el.closest(options.block).length) return;

		$($links.parent()).removeClass('active');

        e.stopPropagation();
	});

	$('.js-scroll a').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('ok');
	})

	$modal.find('.js-scroll').jScrollPane();
};
function counter() {
	console.log('hi')

	var options = {
		block: '.js-counter'
	}

	$('.js-counter-start').on('click', function() {
		var $elem = $(this);
		var $parent = $elem.closest(options.block);


		$parent.addClass('active');
		$elem.css({
			left: $elem.offset().left,
			top: $elem.offset().top,
			position: 'fixed'
		})
		$elem
			.animate({
				left: 1120,
				top: 20
			}, 900, function() {
				$elem.attr('style', '');
			}
		);

		return false;
	});


	$('.js-counter-minus').on('click', function() {
		var $el = $(this);
		var $parent = $el.closest('.js-counter-pane');
		var $viewPort = $parent.find('span');
		var value = +$viewPort.text();

		value > 1
			? $viewPort.text(value-=1)
			: $parent.closest(options.block).removeClass('active');

		return false;
	});
	$('.js-counter-plus').on('click', function() {
		var $el = $(this);
		var $parent = $el.closest('.js-counter-pane');
		var $viewPort = $parent.find('span');
		var value = +$viewPort.text();

		$viewPort.text(value+=1);

		return false;
	});
};
(function() {
	$(document).ready(function() {
	    $('.menutop ul li').click(function() {
	        $.arcticmodal({
	            type: 'ajax',
	            url: $(this).children('a').attr('href'),
	            afterLoading: function(data, el) {
	                $(".arcticmodal-container").css({
	                	top: screen.height,
	                	overflow: 'hidden'
	                });

	            },
	            afterLoadingOnShow: function(data, el) {
	                $(".arcticmodal-container").animate({
	                    top: "-=" + screen.height,
	                }, 1200, function() {
	                	$(".arcticmodal-container").css({
		                	overflow: 'auto'
		                });
	                });

	                bindCloseModal();
	                setCatalogueBehavior();
	                bindSelect();
	                counter();
	                bindMenu();

	                switcher();
	            },
	            beforeClose: function(data, el) {
	            	$(".arcticmodal-container").css({ overflow: 'hidden' });
	                $(".arcticmodal-container").animate({
	                    top: "+=" + screen.height,
	                }, 1200);
	            }
	        });
	    });
	});
	function bindCloseModal() {
		$('.js-close_modal').on('click', function() {
        	$('.arcticmodal-overlay').trigger('click');
    	});
	}
})();


(function() {


	$.fn.navigation = function() {
		var $nav = $(this);
		var $links = $nav.find('a');

		var settings = {
			"showY": 268
		}

		var options = $.extend({}, settings, $nav.data('nav-options'));

		$links.on('click', function(e) {
			e.preventDefault();
		});

		$(window).on('scroll', function() {

			var top = $(document).scrollTop();
			if (top > options.showY)
				$nav.addClass('fixed');
			else
				$nav.removeClass('fixed');
		});

		$('.arcticmodal-container').on('scroll', function() {
			var top = $(this).scrollTop();
			if (top > $options.showY) {
				$nav.addClass('fixed');
				$('.js-fix').addClass('fixed');
			}
			else {
				$nav.removeClass('fixed');
				$('.js-fix').removeClass('fixed');
			}
		});

		console.log(options);

	}

})(jQuery);

function bindMenu() {
	var $block = $('.js-nav');
	$.each($block, function() {
		var $el = $(this);

		$el.navigation();
	});
}

bindMenu();


function bindSelect() {
	var $select = $('.js-select').find('select');
	$select.select2({
		minimumResultsForSearch: -1
	});
};
(function() {

	var options = {
		stopOnHover: true,
		autoplay: 5000,
		slideSpeed : 900,
		paginationSpeed : 500,
		items : 1,
		itemsDesktop : false,
		itemsDesktopSmall : false,
		itemsTablet: false,
		itemsMobile : false
  	}

  	var $block = $(".js-slider");

	$.each($block, function() {
		var $el = $(this);
		var settings = $el.data('slider-settings');

		$el.owlCarousel($.extend({}, options, settings));
	});

 })();
function switcher() {
	var switcher = {
		options: {
			$block: '.js-switcher',
			$bindings: '.js-switcher-button'
		},
		run: function() {
			console.log(this.options.$block)
			$.each($(this.options.$block), function(index, el) {
				var $block = $(el);
				var $elems = $block.find(switcher.options.bindings);

				$elems.on('click', function(e) {
					e.preventDefault();

					$elems.removeClass('active');
					$(this).addClass('active');
				})

				$block.css({
					marginLeft: -$(this).width()/2 + $(this).height()/2,
					marginTop: -$(this).height()/2
				})
			});
		}
	}

	switcher.run();
}
